const { useBase } = require('./base');
const commandLineArgs = require('command-line-args');

const optionDefinitions = [
    { name: 'page', type: Number },
    { name: 'size', type: Number }
];
const options = commandLineArgs(optionDefinitions);

const paginate = async () => {
    try {
        const { base } = await useBase();
        
        const offset = (options.page - 1) * options.size;
        const limit = options.size;

        const videos = await base('videos')
        .select('*')
        .offset(offset)
        .limit(limit);

        console.log(videos);
        process.exit();

    } catch (error) {
        console.error('Error:', error);
    }
};


(async () => {
    await paginate()
})()
