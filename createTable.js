const { useBase } = require('./base');

const createTable = async () => {
    const { base } = await useBase();
    return new Promise(async (resolve, reject) => {
        await base.schema.createTable('videos', table => {
        table.increments('id').primary();
        table.string('title').notNullable();
        table.integer('views').notNullable();
        table.text('category').notNullable();
        resolve("Table created")
        }).catch(err => {
        reject(err)
        })
    })
}

(async () => {
    console.log(await createTable())
})()

