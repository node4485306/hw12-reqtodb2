const { useBase } = require('./base');
const { options } = require('./cli-options')

const insert = async () => {
    try {
        const { base } = await useBase();

        await base('videos').insert({
            title: options.title,
            views: options.views,
            category: options.category
        })
        console.log("Data inserted")
        process.exit();

    } catch (error) {
        console.log(error)
    }
}

(async () => {
    await insert();
})()

