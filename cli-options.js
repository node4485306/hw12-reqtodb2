const commandLineArgs = require('command-line-args');

const optionsDefinition = [ 
    { name: 'id', type: Number },
    { name: 'title', type: String },
    { name: 'views', type: Number },
    { name: 'category', type: String }
]

const options = commandLineArgs(optionsDefinition);

module.exports = { options }