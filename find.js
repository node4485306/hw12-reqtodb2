const { useBase } = require('./base');
const commandLineArgs = require('command-line-args');

const optionDefinitions = [
    { name: 'search', type: String }
];
const options = commandLineArgs(optionDefinitions);

const find = async () => {
    try {
        const { base } = await useBase();

        const videos = await base('videos')
        .select('*')
        .where('title', 'like', `%${options.search}%`)

        console.log(videos);
        process.exit();

    } catch (error) {
        console.error('Error:', error);
    }
};


(async () => {
    await find()
})()
