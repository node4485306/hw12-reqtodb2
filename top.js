const { useBase } = require('./base');
const commandLineArgs = require('command-line-args');

const optionDefinitions = [
    { name: 'top', type: Number }
];
const options = commandLineArgs(optionDefinitions);

const top = async () => {
    try {
        const { base } = await useBase();

        const result = await base('videos')
        .select('category')
        .sum('views as viewsCount')
        .groupBy('category')
        .orderBy('viewsCount', 'desc')
        .limit(options.top);

        console.log('Top categories:', result);
        process.exit();

    } catch (error) {
        console.error('Error:', error);
    }
};


(async () => {
    await top()
})()
