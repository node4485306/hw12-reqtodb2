const { useBase } = require('./base');

const group = async () => {
    try {
        const { base } = await useBase();

        const result = await base('videos')
        .select('*')
        .count('views as viewsCount')
        .groupBy('category');

        console.log(result);
        process.exit();
        
    } catch (error) {
        console.error('Error:', error);
    }
};


(async () => {
    await group()
})()
